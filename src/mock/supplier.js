import Mock from 'mockjs'

const List = []
const array = ['江铃控股', '江铃股份','江西五十铃']
const arraycode = ['jmc', 'jmclandwind', 'isuzu']
const arrayshortname = ['jmc', 'jmclandwind','isuzu']

for (let i = 0; i < array.length; i++ ) {
  let name = array[i]
  let code = arraycode[i]
  let shortName =arrayshortname[i]

  List.push(Mock.mock({
    id: '@increment',
    code: code,
    name : name,
    shortName:shortName,
    genre:'整车',
    rate:'@float(0,17,2,2)',
    address:'江西省南昌市',
    fax:/^0\d{2,3}-?\d{7,8}$/,
    telephone:/^0\d{2,3}-?\d{7,8}$/,
    otherTelephone:/^0\d{2,3}-?\d{7,8}$/,
    contacts:'@cname',
    contactsPhone:/^1[34578]\d{9}$/,
    postcode:'@zip',
    isEnable: '@integer(0, 1)'
  }))
}

export default {
  getList: config => {
    const { current,size,orderByField,condition } = JSON.parse(config.body)

    let mockList = List.filter(item => {
      if(condition !== undefined){
        if (condition.name && item.name !== condition.name) return false
      }
      return true
    })

    const pageList = mockList.filter((item, index) => index < size * current && index >= size * (current - 1))

    return {
      "code": 0,
      "data": {
        "total": mockList.length,
        "size": 10,
        "pages": 0,
        "current": 1,
        "searchCount": true,
        "openSort": true,
        "orderByField": null,
        "records":pageList,
        "condition": {
          "name": "null"
        },
        "asc": true,
        "offsetCurrent": 0
      },
      "message": "success"
    }
  },
  createSupplier: config => {
    const { code,name,isEnable,shortName,genre,rate,address,fax,telephone,otherTelephone,contacts,contactsPhone,postcode} = JSON.parse(config.body)



    return {
      "code": 0,
      "data": {
        "id": 111,
        "code": code,
        "shortName": shortName,
        "name": name,
        "isEnable": isEnable,
        "rate":rate,
        "genre":genre,
        "address":address,
        "fax":fax,
        "telephone":telephone,
        "otherTelephone":otherTelephone,
        "contacts":contacts,
        "contactsPhone":contactsPhone,
        "postcode":postcode
      },
      "message": "success"
    }
  },
  updateSupplier: config => {
    const {id,code,name,isEnable,genre,shortName,rate,address,fax,telephone, otherTelephone,contacts,contactsPhone,postcode } = JSON.parse(config.body)
    return {
      "code": 0,
      "data": {
        "id": id,
        "code": code,
        "shortName": shortName,
        "name": name,
        "isEnable": isEnable,
        "rate":rate,
        "genre":genre,
        "address":address,
        "fax":fax,
        "telephone":telephone,
        "otherTelephone":otherTelephone,
        "contacts":contacts,
        "contactsPhone":contactsPhone,
        "postcode":postcode
      },
      "message": "success"
    }
  },
  deleteSupplier: config => {
    return {
      "code": 0,
      "data": true,
      "message": "success"
    }
  }
}

