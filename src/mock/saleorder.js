import Mock from 'mockjs'

const List = []
const count = 20
const detailList = []


const shops = ['重庆江铃', '重庆怡之风', '四川福顺', '西昌怡之风']

//单据状态 10 已登记 20 已审核 30 已作废
for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    code: '@increment',
    'shopName|1': shops,
    'status|1': ['已登记', '已审核', '已作废'],
    checkStatus:'未审核',
    customName:'@cname',
    cellphone:/^1[34578]\d{9}$/,
    salesConsultant:'@cname',
    'department|1':['销售部', '大客户部', '展厅'],
    'makeoutStatus|1':['已开票','未开票'],
    makeoutor:'@cname',
    'vehicleCode|1':['bd'],
    'outcolor|1':['贵族珍珠调色', '黄白色', '珍珠白色', '红云母金属色','米黄云母色','棕金云母金属色','铂青铜金属色','炫黑云母色','乳白色','驼白色'],
    'incolor|1': ['棕色', '米黄色', '深色', '灰色','浅色','深红色','深空蓝','紫色','黑色'],
    makeoutDate:'@datetime("yyyy-MM-dd HH:mm:ss")',
    makeoutAmount:'@float(60000, 100000, 0, 2)',
    price:function () {
      return (this.standardPrice * 0.1).toFixed(2)
    },
    standardPrice:'@float(60000, 100000, 0, 2)',
    account:function () {
      return this.standardPrice
    },
    insuranceCompany:'',
    'paymentMethod|1':['一次性付款','按揭付款'],
    invoiceCode:'c_'+'@increment',
    vin:'11111111111111111',
    engineCode:'',
    creator: '@cname',
    createTime: '@datetime("yyyy-MM-dd HH:mm:ss")',
    handsel:'@float(60000, 100000, 0, 2)',
    'installment|1':[],
    'licenseNo|1':[],
    brandName: '宝典',
    seriesName: '宝典PLUS版柴油',
    'modelName|1': ['4×2MT(GL)', '4×2MT(GL)', '4×4MT(GL)', '4×4MT(LX)'],
    handsel:'@float(60000, 100000, 0, 2)',
    'buyCarStyle|1':['增购','新购'],
    orderDate:'@datetime("yyyy-MM-dd")',
    operator:'@cname',
    auditTime:'@datetime("yyyy-MM-dd HH:mm:ss")',
    gatherTime:'@datetime("yyyy-MM-dd HH:mm:ss")',
    checkTime:'@datetime("yyyy-MM-dd HH:mm:ss")',
    pickupTime:'@datetime("yyyy-MM-dd HH:mm:ss")',
    outboundTime:'@datetime("yyyy-MM-dd HH:mm:ss")',
    leaveTime:'@datetime("yyyy-MM-dd HH:mm:ss")',
    comment: ''
  }))
}


export default {

  getList: config => {
    const {current, size, orderByField, condition} = JSON.parse(config.body)

    let mockList = List.filter(item => {
      if (condition !== undefined) {
        if (condition.code && item.code !== condition.code) return false
      }
      return true
    })

    const pageList = mockList.filter((item, index) => index < size * current && index >= size * (current - 1))

    return {
      "code": 0,
      "data": {
        "total": mockList.length,
        "size": 10,
        "pages": 0,
        "current": 1,
        "searchCount": true,
        "openSort": true,
        "orderByField": null,
        "records": pageList,
        "condition": {
          "name": "null"
        },
        "asc": true,
        "offsetCurrent": 0
      },
      "message": "success"
    }
  },

  getDetailList: config => {
    const {current, size, orderByField, condition} = JSON.parse(config.body)

    // let mockList = detailList.filter(item => {
    //   if (condition !== undefined) {
    //     if (condition.name && item.name !== condition.name) return false
    //   }
    //   return true
    // })

    const pageList = detailList.filter((item, index) => index < size * current && index >= size * (current - 1))

    return {
      "code": 0,
      "data": {
        "total": detailList.length,
        "size": 10,
        "pages": 0,
        "current": 1,
        "searchCount": true,
        "openSort": true,
        "orderByField": null,
        "records": pageList,
        "condition": {
          "name": "null"
        },
        "asc": true,
        "offsetCurrent": 0
      },
      "message": "success"
    }
  },

  create: config => {
    const {code, checkStatus, status, shopId, whId,supplierId,vehicleType,comment,count,amount,creator,createTime} = JSON.parse(config.body)

    return {
      "code": 0,
      "data": {
        "id": 111,
        "code":code,
        "shopName":'重庆江铃',
        "status": status,
        "checkStatus": checkStatus,
        "shopId":shopId,
        "whId":whId,
        "vehicleType":vehicleType,
        "supplierId":supplierId,
        "comment": comment,
        "count":count,
        "amount":amount,
        "creator":creator,
        "createTime":createTime
      },
      "message": "success"
    }
  },

  update: config => {
    const {id,code, checkStatus, status, shopId, whId,supplierId,vehicleType,comment,count,amount,creator,createTime} = JSON.parse(config.body)

    return {
      "code": 0,
      "data": {
        "id":id,
        "code":code,
        "shopName":'重庆江铃',
        "status": status,
        "checkStatus": checkStatus,
        "shopId":shopId,
        "whId":whId,
        "vehicleType":vehicleType,
        "supplierId":supplierId,
        "comment": comment,
        "count":count,
        "amount":amount,
        "creator":creator,
        "createTime":createTime
      },
      "message": "success"
    }
  },

  delete: config => {
    return {
      "code": 0,
      "data": true,
      "message": "success"
    }
  }

}
