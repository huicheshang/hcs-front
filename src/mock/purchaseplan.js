import Mock from 'mockjs'

const List = []
const count = 20
const detailList = []
const execList = []
const optList = []
const detailEditList = []



const suppliers = ['江铃股份', '江铃控股', '五十铃']
const shops = ['重庆江铃', '重庆怡之风', '四川福顺', '西昌怡之风']

//单据状态 10 已登记 20 已审核 30 已作废

for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    code: 'cg-' + '@increment',
    'shopName|1': shops,
    name: '采购计划' + '@increment',
    'status|1': ['已登记', '已审核', '已作废'],
    finaceCheck: '未审核',
    planPerson: '@cname',
    planDate: '@datetime("yyyy-MM-dd")',
    'supplier|1': suppliers,
    'department|1': ['销售部', '大客户部', '展厅'],
    comment: ''
  }))
}

for (let i = 0; i < count; i++) {
  detailList.push(Mock.mock({
    id: '@increment',
    planId: '@increment',
    brandName: '宝典',
    seriesName: '宝典PLUS版柴油',
    'modelName|1': ['4×2MT(GL)', '4×2MT(GL)', '4×4MT(GL)', '4×4MT(LX)'],
    'colorCode|1': ['001', '002', '003', '004'],
    'colorName|1': ['棕色', '米黄色', '深色', '灰色', '浅色', '深红色', '深空蓝', '紫色', '黑色'],
    'planCount|1-100': 100,
    price: '@float(60000, 100000, 0, 2)',
    amount: function () {
      return this.planCount * this.price
    },
    comment: ''
  }))
}

for (let i = 0; i < count; i++) {
  execList.push(Mock.mock({
    id: '@increment',
    planId: '@increment',
    brandName: '宝典',
    seriesName: '宝典PLUS版柴油',
    'modelName|1': ['4×2MT(GL)', '4×2MT(GL)', '4×4MT(GL)', '4×4MT(LX)'],
    'colorCode|1': ['001', '002', '003', '004'],
    'colorName|1': ['棕色', '米黄色', '深色', '灰色', '浅色', '深红色', '深空蓝', '紫色', '黑色'],
    'planCount|100-500': 100,
    withdrawCount: function () {
      return this.planCount - 10
    },
    shipmentCount: function () {
      return this.planCount - 30
    },
    arrivalCount: function () {
      return this.planCount - 50
    },
    diffCount: function () {
      return this.planCount - this.withdrawCount
    },
    price: '@float(60000, 100000, 0, 2)',
    amount: function () {
      return this.planCount * this.price
    },
    diffamount: function () {
      return this.diffCount * this.price
    },
    comment: ''
  }))
}

export default {
  getList: config => {
    const {current, size, orderByField, condition} = JSON.parse(config.body)

    let mockList = List.filter(item => {
      if (condition !== undefined) {
        if (condition.name && item.name !== condition.name) return false
      }
      return true
    })

    const pageList = mockList.filter((item, index) => index < size * current && index >= size * (current - 1))

    return {
      "code": 0,
      "data": {
        "total": mockList.length,
        "size": 10,
        "pages": 0,
        "current": 1,
        "searchCount": true,
        "openSort": true,
        "orderByField": null,
        "records": pageList,
        "condition": {
          "name": "null"
        },
        "asc": true,
        "offsetCurrent": 0
      },
      "message": "success"
    }
  },

  getDetailLIst: config => {
    const {current, size, orderByField, condition} = JSON.parse(config.body)

    let mockList = detailList.filter(item => {
      if (condition !== undefined) {
        if (condition.name && item.name !== condition.name) return false
      }
      return true
    })

    const pageList = mockList.filter((item, index) => index < size * current && index >= size * (current - 1))

    return {
      "code": 0,
      "data": {
        "total": mockList.length,
        "size": 10,
        "pages": 0,
        "current": 1,
        "searchCount": true,
        "openSort": true,
        "orderByField": null,
        "records": pageList,
        "condition": {
          "name": "null"
        },
        "asc": true,
        "offsetCurrent": 0
      },
      "message": "success"
    }
  },

  getExecList: config => {
    const {current, size, orderByField, condition} = JSON.parse(config.body)

    let mockList = execList.filter(item => {
      if (condition !== undefined) {
        if (condition.name && item.name !== condition.name) return false
      }
      return true
    })

    const pageList = mockList.filter((item, index) => index < size * current && index >= size * (current - 1))

    return {
      "code": 0,
      "data": {
        "total": mockList.length,
        "size": 10,
        "pages": 0,
        "current": 1,
        "searchCount": true,
        "openSort": true,
        "orderByField": null,
        "records": pageList,
        "condition": {
          "name": "null"
        },
        "asc": true,
        "offsetCurrent": 0
      },
      "message": "success"
    }
  },

  getOptList: config => {
    const {current, size, orderByField, condition} = JSON.parse(config.body)

    let mockList = optList.filter(item => {
      if (condition !== undefined) {
        if (condition.name && item.name !== condition.name) return false
      }
      return true
    })

    const pageList = mockList.filter((item, index) => index < size * current && index >= size * (current - 1))

    return {
      "code": 0,
      "data": {
        "total": mockList.length,
        "size": 10,
        "pages": 0,
        "current": 1,
        "searchCount": true,
        "openSort": true,
        "orderByField": null,
        "records": pageList,
        "condition": {
          "name": "null"
        },
        "asc": true,
        "offsetCurrent": 0
      },
      "message": "success"
    }
  },


  getDEList: config => {
    const {current, size, orderByField, condition} = JSON.parse(config.body)

    let mockList = detailEditList.filter(item => {
      if (condition !== undefined) {
        if (condition.name && item.name !== condition.name) return false
      }
      return true
    })

    const pageList = mockList.filter((item, index) => index < size * current && index >= size * (current - 1))

    return {
      "code": 0,
      "data": {
        "total": mockList.length,
        "size": 10,
        "pages": 0,
        "current": 1,
        "searchCount": true,
        "openSort": true,
        "orderByField": null,
        "records": pageList,
        "condition": {
          "name": "null"
        },
        "asc": true,
        "offsetCurrent": 0
      },
      "message": "success"
    }
  },

  createPlan: config => {
    const {code, name, finaceCheck, status, planPerson, comment,planDate,supplier,department,shopName} = JSON.parse(config.body)

    return {
      "code": 0,
      "data": {
        "id": 111,
        "code":code,
        "name":name,
        "status": status,
        "finaceCheck": finaceCheck,
        "planPerson":planPerson,
        "shopName":shopName,
        "planDate":planDate,
        "supplier":supplier,
        "department":department,
        "comment": comment
      },
      "message": "success"
    }
  },
  updatePlan: config => {
    const {id,code, name, finaceCheck, status, planPerson, comment,planDate,supplier,department,shopName} = JSON.parse(config.body)

    return {
      "code": 0,
      "data": {
        "id": id,
        "code":code,
        "name":name,
        "status": status,
        "finaceCheck": finaceCheck,
        "planPerson":planPerson,
        "shopName":shopName,
        "planDate":planDate,
        "supplier":supplier,
        "department":department,
        "comment": comment
      },
      "message": "success"
    }
  },
  deletePlan: config => {
    return {
      "code": 0,
      "data": true,
      "message": "success"
    }
  }
}
