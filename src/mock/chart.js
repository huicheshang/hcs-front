import Mock from 'mockjs'
import { param2Obj } from '@/utils'


export default {

  getCurrentDayCount: config => {
    //const {currentDate} = JSON.parse(config.body)
    const {currentDate} = param2Obj(config.url);
    const returnData = [
      {name:'今日营业额',value:'12', history:'11'},
      {name:'今日产值',value:'12', history:'11'},
      {name:'销售总车辆',value:'12'},
      {name:'今日接待客户',value:'12', history:'11'},
      {name:'今日意向客户',value:'12', history:'11'},
      {name:'今日车辆订单',value:'12', history:'11'},
      {name:'今日车辆销售',value:'12', history:'11'},
      {name:'当前库存车辆',value:'12'},
    ];

    return {
      "code": 0,
      "data": returnData,
      "message": "success",
    }
  }
  ,getCurrentMonthCount: config => {
    //const {currentDate} = JSON.parse(config.body)
    const {currentMonth} = param2Obj(config.url);
    const returnData = [
      {name:'本月营业额',data:[20,30,68,985,252,20,30,68,985,252,20,30,68,985,252,20,30,68,985,252,20,30,68,985,252,20,30,68,985,252,123]}
    ];

    return {
      "code": 0,
      "data": returnData,
      "message": "success"
    }
  }
  ,getCurrentYearCount: config => {
    //const {currentDate} = JSON.parse(config.body)
    const {currentYear} = param2Obj(config.url)
    const returnData = [
      {name:'本年营业额', data:[240,320,53,985,252,240,320,53,985,252,234,324]}
    ];

    return {
      "code": 0,
      "data": returnData,
      "message": "success"
    }
  }
  ,getHistoryCount: config => {
    const returnData = [{name:'历史营业额',data:[1221,21321,21321]},{name:'年份', data:[2014,2015,2018]}]

    return {
      "code": 0,
      "data": returnData,
      "message": "success"
    }
  }
  ,getWarehouseCount: config => {
    const returnData = [
        {name:'含税金额',data:[1221,21321,31321,1221,51321,21321,1221,21321,41321,1221,21321,21321]},
        {name:'剔税金额', data:[1100,21000,30999,1100,51000,20999,1100,21000,40999,1100,21000,20999]},
        {name:'结存数量', data:[54,78,99,54,78,99,54,78,99,54,78,130]}
      ];

    return {
      "code": 0,
      "data": returnData,
      "message": "success"
    }
  }
  ,getSaleCount: config => {
    const returnData = [
      {name:'含税金额',data:[1221,71321,21321,1221,21321,21321,4221,21321,21321,1221,21321,21321]},
      {name:'剔税金额', data:[1100,71000,20999,1100,21000,20999,4100,21000,20999,1100,21000,20999]},
      {name:'结存数量', data:[54,78,99,54,78,99,54,78,99,54,78,99]}
      ];

    return {
      "code": 0,
      "data": returnData,
      "message": "success"
    }
  }
}
