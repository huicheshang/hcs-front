import Mock from 'mockjs'

const List = []
const count = 20


const shops = ['重庆江铃', '重庆怡之风', '四川福顺', '西昌怡之风']


for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    code: '@increment',
    'orgName|1': shops,
    vin:'',
    orderCode:'@increment',
    engineNo:'',
    'status|1': ['在库','锁定'],
    supplyStatus:'未配车',
    'isTest|1':['是','否'],
    'isShow|1':['是','否'],
    'colorName|1':['贵族珍珠调色', '黄白色', '珍珠白色', '红云母金属色','米黄云母色','棕金云母金属色','铂青铜金属色','炫黑云母色','乳白色','驼白色'],
    'customName':'@cname',
    invoiceor:'@cname',
    sales:'@cname',
    locker:''
  }))
}




export default {
  getList: config => {
    const {current, size, orderByField, condition} = JSON.parse(config.body)

    let mockList = List.filter(item => {
      if (condition !== undefined) {
        if (condition.code && item.code !== condition.code) return false
      }
      return true
    })
    const pageList = mockList.filter((item, index) => index < size * current && index >= size * (current - 1))
    return {
      "code": 0,
      "data": {
        "total": mockList.length,
        "size": 10,
        "pages": 0,
        "current": 1,
        "searchCount": true,
        "openSort": true,
        "orderByField": null,
        "records": pageList,
        "condition": {
          "name": "null"
        },
        "asc": true,
        "offsetCurrent": 0
      },
      "message": "success"
    }
  }
}
