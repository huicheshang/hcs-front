import Mock from 'mockjs'

const List = []
const count = 20
const detailList = []


const suppliers = ['江铃股份', '江铃控股', '五十铃']
const shops = ['重庆江铃', '重庆怡之风', '四川福顺', '西昌怡之风']
const warehouse = ['仓库01','仓库02','仓库03']


//单据状态 10 已登记 20 已审核 30 已作废
for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    code: '@increment',
    allotCode: '@increment',
    'shopName|1': shops,
    'whId|1': warehouse,
    'status|1': ['已登记', '已审核', '已作废'],
    checkStatus: '未审核',
    count:'@integer(1, 20)',
    amount:'@float(60000, 100000, 0, 2)',
    'allotFromOrg|1':shops,
    creator: '@cname',
    createTime: '@datetime("yyyy-MM-dd HH:mm:ss")',
    inboundTime:'@datetime("yyyy-MM-dd HH:mm:ss")',
    vehicleType:"10",
    comment: ''
  }))
}


for (let i = 0; i < count; i++) {
  detailList.push(Mock.mock({
    id: '@increment',
    code: '@increment',
    'supplierId|1': suppliers,
    vin:'11111111111111111',
    engineNo:'',
    brandName: '宝典',
    seriesName: '宝典PLUS版柴油',
    'modelName|1': ['4×2MT(GL)', '4×2MT(GL)', '4×4MT(GL)', '4×4MT(LX)'],
    'outcolor|1':['贵族珍珠调色', '黄白色', '珍珠白色', '红云母金属色','米黄云母色','棕金云母金属色','铂青铜金属色','炫黑云母色','乳白色','驼白色'],
    'incolor|1': ['棕色', '米黄色', '深色', '灰色','浅色','深红色','深空蓝','紫色','黑色'],
    warehouseId: '仓库01',
    'locationId':(i+1).toString(),
    amount:'@float(60000, 100000, 0, 2)',
    rate:10,
    rejectRateAmount:function () {
      return (this.amount * 0.9).toFixed(2)
    },
    comment: ''
  }))
}


export default {

  getList: config => {
    const {current, size, orderByField, condition} = JSON.parse(config.body)

    let mockList = List.filter(item => {
      if (condition !== undefined) {
        if (condition.code && item.code !== condition.code) return false
      }
      return true
    })

    const pageList = mockList.filter((item, index) => index < size * current && index >= size * (current - 1))

    return {
      "code": 0,
      "data": {
        "total": mockList.length,
        "size": 10,
        "pages": 0,
        "current": 1,
        "searchCount": true,
        "openSort": true,
        "orderByField": null,
        "records": pageList,
        "condition": {
          "name": "null"
        },
        "asc": true,
        "offsetCurrent": 0
      },
      "message": "success"
    }
  },

  getDetailList: config => {
    const {current, size, orderByField, condition} = JSON.parse(config.body)

    // let mockList = detailList.filter(item => {
    //   if (condition !== undefined) {
    //     if (condition.name && item.name !== condition.name) return false
    //   }
    //   return true
    // })

    const pageList = detailList.filter((item, index) => index < size * current && index >= size * (current - 1))

    return {
      "code": 0,
      "data": {
        "total": detailList.length,
        "size": 10,
        "pages": 0,
        "current": 1,
        "searchCount": true,
        "openSort": true,
        "orderByField": null,
        "records": pageList,
        "condition": {
          "name": "null"
        },
        "asc": true,
        "offsetCurrent": 0
      },
      "message": "success"
    }
  },

  create: config => {
    const {code, checkStatus, status, shopId, whId,supplierId,vehicleType,comment,count,amount,creator,createTime} = JSON.parse(config.body)

    return {
      "code": 0,
      "data": {
        "id": 111,
        "code":code,
        "shopName":'重庆江铃',
        "status": status,
        "checkStatus": checkStatus,
        "shopId":shopId,
        "whId":whId,
        "vehicleType":vehicleType,
        "supplierId":supplierId,
        "comment": comment,
        "count":count,
        "amount":amount,
        "creator":creator,
        "createTime":createTime
      },
      "message": "success"
    }
  },

  update: config => {
    const {id,code, checkStatus, status, shopId, whId,supplierId,vehicleType,comment,count,amount,creator,createTime} = JSON.parse(config.body)

    return {
      "code": 0,
      "data": {
        "id":id,
        "code":code,
        "shopName":'重庆江铃',
        "status": status,
        "checkStatus": checkStatus,
        "shopId":shopId,
        "whId":whId,
        "vehicleType":vehicleType,
        "supplierId":supplierId,
        "comment": comment,
        "count":count,
        "amount":amount,
        "creator":creator,
        "createTime":createTime
      },
      "message": "success"
    }
  },

  delete: config => {
    return {
      "code": 0,
      "data": true,
      "message": "success"
    }
  }

}
