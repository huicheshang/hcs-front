import Mock from 'mockjs'
import intColor from './intColor'
import extColor from './extColor'
import post from './post'
import supplier from './supplier'
import purchaseplan from './purchaseplan'
import inboundpur from './inboundpur'
import inboundallot from './inboundallot'
import outboundsale from './outboundsale'
import outboundallot from './outboundallot'
import inventoryQuery from './inventoryQuery'
import reception from './reception'
import candidate from './candidate'
import advorder from './advorder'
import saleorder from './saleorder'
import puchaseOrder from './puchaseOrder'
import chart from './chart'




// import loginAPI from './login'


// Mock.setup({
//   timeout: '350-600'
// })

// // 登录相关
// Mock.mock(/\/user\/login/, 'post', loginAPI.login)
// Mock.mock(/\/user\/logout/, 'post', loginAPI.logout)
// Mock.mock(/\/user\/info\.*/, 'get', loginAPI.getInfo)

//内饰颜色
Mock.mock(/\/intcolor\/queryPage/, 'post', intColor.getList)
Mock.mock(/\/intcolor\/create/, 'post', intColor.createIntColor)
Mock.mock(/\/intcolor\/update/, 'post', intColor.updateIntColor)
Mock.mock(/\/intcolor\/delete/, 'post', intColor.deleteIntColor)

//外观颜色
Mock.mock(/\/extcolor\/queryPage/, 'post', extColor.getList)
Mock.mock(/\/extcolor\/create/, 'post', extColor.createExtColor)
Mock.mock(/\/extcolor\/update/, 'post', extColor.updateExtColor)
Mock.mock(/\/extcolor\/delete/, 'post', extColor.deleteExtColor)

//岗位
Mock.mock(/\/post\/queryPage/, 'post', post.getList)
Mock.mock(/\/post\/create/, 'post', post.createPost)
Mock.mock(/\/post\/update/, 'post', post.updatePost)
Mock.mock(/\/post\/delete/, 'post', post.deletePost)

//供应商
Mock.mock(/\/supplier\/queryPage/, 'post', supplier.getList)
Mock.mock(/\/supplier\/create/, 'post', supplier.createSupplier)
Mock.mock(/\/supplier\/update/, 'post', supplier.updateSupplier)
Mock.mock(/\/supplier\/delete/, 'post', supplier.deleteSupplier)

//采购计划
Mock.mock(/\/purchasePlan\/queryPage/, 'post', purchaseplan.getList)
Mock.mock(/\/purchasePlan\/detailPage/, 'post', purchaseplan.getDetailLIst)
Mock.mock(/\/purchasePlan\/execPage/, 'post', purchaseplan.getExecList)
Mock.mock(/\/purchasePlan\/optPage/, 'post', purchaseplan.getOptList)
Mock.mock(/\/purchasePlan\/detailEditPage/, 'post', purchaseplan.getDEList)
Mock.mock(/\/purchasePlan\/create/, 'post', purchaseplan.createPlan)
Mock.mock(/\/purchasePlan\/update/, 'post', purchaseplan.updatePlan)
Mock.mock(/\/purchasePlan\/delete/, 'post', purchaseplan.deletePlan)

//采购入库
Mock.mock(/\/inbound\/pur\/queryPage/, 'post', inboundpur.getList)
Mock.mock(/\/inbound\/pur\/create/, 'post', inboundpur.create)
Mock.mock(/\/inbound\/pur\/update/, 'post', inboundpur.update)
Mock.mock(/\/inbound\/pur\/delete/, 'post', inboundpur.delete)
Mock.mock(/\/inbound\/pur\/detailqueryPage/, 'post', inboundpur.getDetailList)

//调拨入库
Mock.mock(/\/inbound\/allot\/queryPage/, 'post', inboundallot.getList)
Mock.mock(/\/inbound\/allot\/create/, 'post', inboundallot.create)
Mock.mock(/\/inbound\/allot\/update/, 'post', inboundallot.update)
Mock.mock(/\/inbound\/allot\/delete/, 'post', inboundallot.delete)
Mock.mock(/\/inbound\/allot\/detailqueryPage/, 'post', inboundallot.getDetailList)

//销售出库
Mock.mock(/\/outbound\/sale\/queryPage/, 'post', outboundsale.getList)
Mock.mock(/\/outbound\/sale\/create/, 'post', outboundsale.create)
Mock.mock(/\/outbound\/sale\/update/, 'post', outboundsale.update)
Mock.mock(/\/outbound\/sale\/delete/, 'post', outboundsale.delete)
Mock.mock(/\/outbound\/sale\/detailqueryPage/, 'post', outboundsale.getDetailList)

//调拨出库
Mock.mock(/\/outbound\/allot\/queryPage/, 'post', outboundallot.getList)
Mock.mock(/\/outbound\/allot\/create/, 'post', outboundallot.create)
Mock.mock(/\/outbound\/allot\/update/, 'post', outboundallot.update)
Mock.mock(/\/outbound\/allot\/delete/, 'post', outboundallot.delete)
Mock.mock(/\/outbound\/allot\/detailqueryPage/, 'post', outboundallot.getDetailList)

//库存查询
Mock.mock(/\/inventoryQuery\/queryPage/, 'post', inventoryQuery.getList)

//展厅接待
Mock.mock(/\/presale\/reception\/queryPage/, 'post', reception.getList)
Mock.mock(/\/presale\/reception\/create/, 'post', reception.create)
Mock.mock(/\/presale\/reception\/update/, 'post', reception.update)
Mock.mock(/\/presale\/reception\/delete/, 'post', reception.delete)
Mock.mock(/\/presale\/reception\/detailqueryPage/, 'post', reception.getDetailList)

//意向客户管理
Mock.mock(/\/presale\/candidate\/queryPage/, 'post', candidate.getList)
Mock.mock(/\/presale\/candidate\/create/, 'post', candidate.create)
Mock.mock(/\/presale\/candidate\/update/, 'post', candidate.update)
Mock.mock(/\/presale\/candidate\/delete/, 'post', candidate.delete)
Mock.mock(/\/presale\/candidate\/detailqueryPage/, 'post', candidate.getDetailList)

//预订单
Mock.mock(/\/process\/advorder\/queryPage/, 'post', advorder.getList)
Mock.mock(/\/process\/advorder\/create/, 'post', advorder.create)
Mock.mock(/\/process\/advorder\/update/, 'post', advorder.update)
Mock.mock(/\/process\/advorder\/delete/, 'post', advorder.delete)
Mock.mock(/\/process\/advorder\/detailqueryPage/, 'post', advorder.getDetailList)

//订单
Mock.mock(/\/process\/saleorder\/queryPage/, 'post', saleorder.getList)
Mock.mock(/\/process\/saleorder\/create/, 'post', saleorder.create)
Mock.mock(/\/process\/saleorder\/update/, 'post', saleorder.update)
Mock.mock(/\/process\/saleorder\/delete/, 'post', saleorder.delete)
Mock.mock(/\/process\/saleorder\/detailqueryPage/, 'post', saleorder.getDetailList)

//采购订单
Mock.mock(/\/purchase\/order\/queryPage/, 'post', puchaseOrder.getList)
Mock.mock(/\/purchase\/order\/create/, 'post', puchaseOrder.create)
Mock.mock(/\/purchase\/order\/update/, 'post', puchaseOrder.update)
Mock.mock(/\/purchase\/order\/delete/, 'post', puchaseOrder.delete)
Mock.mock(/\/purchase\/order\/detailqueryPage/, 'post', puchaseOrder.getDetailList)

// chart
Mock.mock(/\/chart\/getCurrentDayCount/, 'get', chart.getCurrentDayCount)
Mock.mock(/\/chart\/getCurrentMonthCount/, 'get', chart.getCurrentMonthCount)
Mock.mock(/\/chart\/getCurrentYearCount/, 'get', chart.getCurrentYearCount)
Mock.mock(/\/chart\/getHistoryCount/, 'get', chart.getHistoryCount)
Mock.mock(/\/chart\/getWarehouseCount/, 'get', chart.getWarehouseCount)
Mock.mock(/\/chart\/getSaleCount/, 'get', chart.getSaleCount)

export default Mock
