import Mock from 'mockjs'

const List = []
const count = 20
const detailList = []

const suppliers = ['江铃股份', '江铃控股', '五十铃']
const shops = ['重庆江铃', '重庆怡之风', '四川福顺', '西昌怡之风']
const departments = ['销售部', '大客户部', '展厅']


for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: i,
    code: '@increment',
    'shopName|1': shops,
    'status|1': ['已登记', '已审核', '已作废'],
    finaceCheck: '未审核',
    planCode: '@increment',
    count: '@integer(1, 20)',
    amount: '@float(60000, 100000, 0, 2)',
    creator: '@cname',
    createDate: '@datetime("yyyy-MM-dd")',
    'department|1': departments,
    'supplier|1': suppliers,
    comment: ''
  }))
}

for (let i = 0; i < count; i++) {
    detailList.push(Mock.mock({
      id: '@increment',
      brandName: '宝典',
      seriesName: '宝典PLUS版柴油',
      'modelName|1': ['4×2MT(GL)', '4×2MT(GL)', '4×4MT(GL)', '4×4MT(LX)'],
      colorCode: 'color-'+ (i+1).toString(),
      'colorName|1': ['棕色', '米黄色', '深色', '灰色','浅色','深红色','深空蓝','紫色','黑色'],
      count:'@integer(1, 20)',
      planCount:'@integer(20, 30)',
      arrivalCount:'@integer(1, 10)',
      diffCount: function() {
        return this.planCount - this.arrivalCount - this.count
      },
      price: '@float(60000, 100000, 0, 2)',
      amount: function () {
        return (this.price * this.count).toFixed(2)
      },
      planAmount:function () {
        return (this.price * this.planCount).toFixed(2)
      },
      comment:'',
      pid:i
    }))
}

export default {

  getList: config => {
    const {current, size, orderByField, condition} = JSON.parse(config.body)

    let mockList = List.filter(item => {
      if (condition !== undefined) {
        if (condition.code && item.code !== condition.code) return false
      }
      return true
    })

    const pageList = mockList.filter((item, index) => index < size * current && index >= size * (current - 1))

    return {
      "code": 0,
      "data": {
        "total": mockList.length,
        "size": 10,
        "pages": 0,
        "current": 1,
        "searchCount": true,
        "openSort": true,
        "orderByField": null,
        "records": pageList,
        "condition": {
          "name": "null"
        },
        "asc": true,
        "offsetCurrent": 0
      },
      "message": "success"
    }
  },

  getDetailList: config => {
    const {current, size, orderByField, condition} = JSON.parse(config.body)

    let mockList =  detailList.filter(item => item.pid == condition.id)

    console.log(condition)

    const pageList = mockList.filter((item, index) => index < size * current && index >= size * (current - 1))

    return {
      "code": 0,
      "data": {
        "total": mockList.length,
        "size": 10,
        "pages": 0,
        "current": 1,
        "searchCount": true,
        "openSort": true,
        "orderByField": null,
        "records": pageList,
        "condition": {
          "name": "null"
        },
        "asc": true,
        "offsetCurrent": 0
      },
      "message": "success"
    }
  },

  create: config => {
    const {code, checkStatus, status, shopName, allotToOrg, whId, outboundTime, comment, count, amount, creator, createTime} = JSON.parse(config.body)

    return {
      "code": 0,
      "data": {
        "id": 111,
        "code": code,
        "shopName": shopName,
        "status": status,
        "checkStatus": checkStatus,
        "whId": whId,
        "comment": comment,
        "count": count,
        "amount": amount,
        "allotToOrg": allotToOrg,
        "isAllout": isAllout,
        "creator": creator,
        "createTime": createTime,
        "outboundTime": outboundTime
      },
      "message": "success"
    }
  },

  update: config => {
    const {id, code, checkStatus, status, shopName, allotToOrg, whId, outboundTime, comment, count, amount, creator, createTime} = JSON.parse(config.body)

    return {
      "code": 0,
      "data": {
        "id": id,
        "code": code,
        "shopName": shopName,
        "status": status,
        "checkStatus": checkStatus,
        "whId": whId,
        "comment": comment,
        "count": count,
        "amount": amount,
        "allotToOrg": allotToOrg,
        "isAllout": isAllout,
        "creator": creator,
        "createTime": createTime,
        "outboundTime": outboundTime
      },
      "message": "success"
    }
  },

  delete: config => {
    return {
      "code": 0,
      "data": true,
      "message": "success"
    }
  }

}
