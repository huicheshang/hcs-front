import Mock from 'mockjs'
import { param2Obj } from '@/utils'

const List = []
const count = 20

for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    code:'color_'+i.toString(),
    'name|1': ['棕色', '米黄色', '深色', '灰色','浅色','深红色','深空蓝','紫色','黑色'],
    sort: '@integer(1, 20)',
    isEnable: '@integer(0, 1)',
    comment: '@ctitle(10, 20)'
  }))
}

export default {
  getList: config => {
    const { current,size,orderByField,condition } = JSON.parse(config.body)

    let mockList = List.filter(item => {
      if(condition !== undefined){
        if (condition.name && item.name !== condition.name) return false
      }
      return true
    })

    const pageList = mockList.filter((item, index) => index < size * current && index >= size * (current - 1))

    return {
      "code": 0,
      "data": {
        "total": mockList.length,
        "size": 10,
        "pages": 0,
        "current": 1,
        "searchCount": true,
        "openSort": true,
        "orderByField": null,
        "records":pageList,
        "condition": {
          "name": "null"
        },
        "asc": true,
        "offsetCurrent": 0
      },
      "message": "success"
    }
  },
  createIntColor: config => {
    const { code,name,isEnable,sort,comment } = JSON.parse(config.body)
    return {
      "code": 0,
      "data": {
        "id": 111,
        "code": code,
        "name": name,
        "sort": sort,
        "isEnable": isEnable,
        "comment":comment
      },
      "message": "success"
    }
  },
  updateIntColor: config => {
    const { id,code,name,isEnable,sort,comment } = JSON.parse(config.body)
    return {
      "code": 0,
      "data": {
        "id": id,
        "code": code,
        "name": name,
        "sort":sort,
        "isEnable": isEnable,
        "comment":comment
      },
      "message": "success"
    }
  },
  deleteIntColor: config => {
    return {
      "code": 0,
      "data": true,
      "message": "success"
    }
  }
}

