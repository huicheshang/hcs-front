import Mock from 'mockjs'

const List = []
const count = 20

for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    code:'@word',
    'name|1': ['贵族珍珠调色', '黄白色', '珍珠白色', '红云母金属色','米黄云母色','棕金云母金属色','铂青铜金属色','炫黑云母色','乳白色','驼白色'],
    sort: '@integer(1, 20)',
    isEnable: '@integer(0, 1)',
    comment: '@ctitle(10, 20)'
  }))
}

export default {
  getList: config => {
    const { current,size,orderByField,condition } = JSON.parse(config.body)

    let mockList = List.filter(item => {
      if(condition !== undefined){
        if (condition.name && item.name !== condition.name) return false
      }
      return true
    })

    const pageList = mockList.filter((item, index) => index < size * current && index >= size * (current - 1))

    return {
      "code": 0,
      "data": {
        "total": mockList.length,
        "size": 10,
        "pages": 0,
        "current": 1,
        "searchCount": true,
        "openSort": true,
        "orderByField": null,
        "records":pageList,
        "condition": {
          "name": "null"
        },
        "asc": true,
        "offsetCurrent": 0
      },
      "message": "success"
    }
  },
  createExtColor: config => {
    const { code,name,isEnable,sort,comment } = JSON.parse(config.body)
    return {
      "code": 0,
      "data": {
        "id": 111,
        "code": code,
        "sort": sort,
        "name": name,
        "isEnable": isEnable,
        "comment":comment
      },
      "message": "success"
    }
  },
  updateExtColor: config => {
    const { id,code,name,isEnable,sort,comment } = JSON.parse(config.body)
    return {
      "code": 0,
      "data": {
        "id": id,
        "code": code,
        "sort": sort,
        "name": name,
        "isEnable": isEnable,
        "comment":comment
      },
      "message": "success"
    }
  },
  deleteExtColor: config => {
    return {
      "code": 0,
      "data": true,
      "message": "success"
    }
  }
}

