import Mock from 'mockjs'

const List = []
const count = 20
const detailList = []

const shops = ['重庆江铃', '重庆怡之风', '四川福顺', '西昌怡之风']
const customSource = ['路过','老客户介绍','老客户增换购(含家庭)','客户朋友介绍','微信、QQ或微博']
const contactTypes = ['首次自行来店','首次来电','外拓搜集','各类介绍主动致电（但未来店）','车展搜集','二次及以上来电',
  '二次及以上自行来店','E-MAIL','网络及新媒体搜集','老客户增换购']

//单据状态 10 已登记 20 已审核 30 已作废
for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    code: '@increment',
    'shopName|1': shops,
    'status|1': ['未订购', '已订购'],
    'checkStatus|1': ['已审核','已登记','已作废'],
    customName:'@cname',
    customPhone: /^1[34578]\d{9}$/,
    'expecttage|1': ['P1(一周内订车)','P2(一月内订车)','P3(三月内订车)','P4(三月以上订车)'],
    brandName: '宝典',
    seriesName: '宝典PLUS版柴油',
    'modelName|1': ['4×2MT(GL)', '4×2MT(GL)', '4×4MT(GL)', '4×4MT(LX)'],
    desolateDays:'@integer(1, 30)',
    salesConsultant:'@cname',
    'department|1':['销售部', '大客户部', '展厅'],
    'firstCome|1':['是','否'],
    reminderTime:'@datetime("yyyy-MM-dd HH:mm:ss")',
    followTimes:'@integer(1, 10)',
    lastfollowDate:'@datetime("yyyy-MM-dd")',
    createTime:'@datetime("yyyy-MM-dd HH:mm:ss")',
    'customSource|1':customSource,
    'contactType|1':contactTypes,
    'isExperience|1':['是','否'],
    receptionor:'@cname',
    province:'@province',
    city:'@city',
    county:'@county',
    street:'',
    description:'',
    defeatTime:'',
    defeatReason:'',
    defeatComment:'',
    defeatResult:'',
    defeator:'',
    comment: ''
  }))
}

detailList.push(Mock.mock({
  id: '@increment',
  code: '@increment',
  'shopName|1': shops,
  salesConsultant:'@cname',
  'status|1':['已审核','已登记','已作废'],
  'department|1':['销售部', '大客户部', '展厅'],
  createTime:'@datetime("yyyy-MM-dd HH:mm:ss")',
  comment:''
}))


export default {

  getList: config => {
    const {current, size, orderByField, condition} = JSON.parse(config.body)

    let mockList = List.filter(item => {
      if (condition !== undefined) {
        if (condition.code && item.code !== condition.code) return false
      }
      return true
    })

    const pageList = mockList.filter((item, index) => index < size * current && index >= size * (current - 1))

    return {
      "code": 0,
      "data": {
        "total": mockList.length,
        "size": 10,
        "pages": 0,
        "current": 1,
        "searchCount": true,
        "openSort": true,
        "orderByField": null,
        "records": pageList,
        "condition": {
          "name": "null"
        },
        "asc": true,
        "offsetCurrent": 0
      },
      "message": "success"
    }
  },

  getDetailList: config => {
    const {current, size, orderByField, condition} = JSON.parse(config.body)

    let mockList = detailList.filter(item => {
      if (condition !== undefined) {
        if (condition.code && item.code !== condition.code) return false
      }
      return true
    })

    const pageList = detailList.filter((item, index) => index < size * current && index >= size * (current - 1))

    return {
      "code": 0,
      "data": {
        "total": detailList.length,
        "size": 10,
        "pages": 0,
        "current": 1,
        "searchCount": true,
        "openSort": true,
        "orderByField": null,
        "records": pageList,
        "condition": {
          "name": "null"
        },
        "asc": true,
        "offsetCurrent": 0
      },
      "message": "success"
    }
  },

  create: config => {
    const {code, checkStatus, status, shopId, whId,supplierId,vehicleType,comment,count,amount,creator,createTime} = JSON.parse(config.body)

    return {
      "code": 0,
      "data": {
        "id": 111,
        "code":code,
        "shopName":'重庆江铃',
        "status": status,
        "checkStatus": checkStatus,
        "shopId":shopId,
        "whId":whId,
        "vehicleType":vehicleType,
        "supplierId":supplierId,
        "comment": comment,
        "count":count,
        "amount":amount,
        "creator":creator,
        "createTime":createTime
      },
      "message": "success"
    }
  },

  update: config => {
    const {id,code, checkStatus, status, shopId, whId,supplierId,vehicleType,comment,count,amount,creator,createTime} = JSON.parse(config.body)

    return {
      "code": 0,
      "data": {
        "id":id,
        "code":code,
        "shopName":'重庆江铃',
        "status": status,
        "checkStatus": checkStatus,
        "shopId":shopId,
        "whId":whId,
        "vehicleType":vehicleType,
        "supplierId":supplierId,
        "comment": comment,
        "count":count,
        "amount":amount,
        "creator":creator,
        "createTime":createTime
      },
      "message": "success"
    }
  },

  delete: config => {
    return {
      "code": 0,
      "data": true,
      "message": "success"
    }
  }

}
