import fetch from '@/utils/fetch'

export function list(listQuery) {
  return fetch({
    url: '/outbound/sale/queryPage',
    method: 'post',
    data: listQuery
  })
}

export function create(info) {
  return fetch({
    url: '/outbound/sale/create',
    method: 'post',
    data: info
  })
}

export function update(info) {
  return fetch({
    url: '/outbound/sale/update',
    method: 'post',
    data: info
  })
}

export function deleted(list) {
  return fetch({
    url: '/outbound/sale/delete',
    method: 'post',
    data: list
  })
}


export function listDetail(listQuery) {
  return fetch({
    url: '/outbound/sale/detailqueryPage',
    method: 'post',
    data: listQuery
  })
}
