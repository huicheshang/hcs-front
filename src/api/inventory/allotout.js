import fetch from '@/utils/fetch'

export function list(listQuery) {
  return fetch({
    url: '/outbound/allot/queryPage',
    method: 'post',
    data: listQuery
  })
}

export function create(info) {
  return fetch({
    url: '/outbound/allot/create',
    method: 'post',
    data: info
  })
}

export function update(info) {
  return fetch({
    url: '/outbound/allot/update',
    method: 'post',
    data: info
  })
}

export function deleted(list) {
  return fetch({
    url: '/outbound/allot/delete',
    method: 'post',
    data: list
  })
}


export function listDetail(listQuery) {
  return fetch({
    url: '/outbound/allot/detailqueryPage',
    method: 'post',
    data: listQuery
  })
}
