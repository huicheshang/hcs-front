import fetch from '@/utils/fetch'

export function list(listQuery) {
  return fetch({
    url: '/inventoryQuery/queryPage',
    method: 'post',
    data: listQuery
  })
}
