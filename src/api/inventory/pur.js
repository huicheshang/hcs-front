import fetch from '@/utils/fetch'

export function list(listQuery) {
  return fetch({
    url: '/inbound/pur/queryPage',
    method: 'post',
    data: listQuery
  })
}

export function create(info) {
  return fetch({
    url: '/inbound/pur/create',
    method: 'post',
    data: info
  })
}

export function update(info) {
  return fetch({
    url: '/inbound/pur/update',
    method: 'post',
    data: info
  })
}

export function deleted(list) {
  return fetch({
    url: '/inbound/pur/delete',
    method: 'post',
    data: list
  })
}


export function listDetail(listQuery) {
  return fetch({
    url: '/inbound/pur/detailqueryPage',
    method: 'post',
    data: listQuery
  })
}
