import fetch from '@/utils/fetch'

export function queryPage(data) {
  return fetch({
    url: '/log/queryPage',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return fetch({
    url: '/log/deleteLog',
    method: 'post',
    data
  })
}

