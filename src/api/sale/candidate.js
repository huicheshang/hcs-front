import fetch from '@/utils/fetch'

export function list(listQuery) {
  return fetch({
    url: '/presale/candidate/queryPage',
    method: 'post',
    data: listQuery
  })
}

export function create(info) {
  return fetch({
    url: '/presale/candidate/create',
    method: 'post',
    data: info
  })
}

export function update(info) {
  return fetch({
    url: '/presale/candidate/update',
    method: 'post',
    data: info
  })
}

export function deleted(list) {
  return fetch({
    url: '/presale/candidate/delete',
    method: 'post',
    data: list
  })
}


export function listDetail(listQuery) {
  return fetch({
    url: '/presale/candidate/detailqueryPage',
    method: 'post',
    data: listQuery
  })
}
