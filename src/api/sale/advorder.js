import fetch from '@/utils/fetch'

export function list(listQuery) {
  return fetch({
    url: '/process/advorder/queryPage',
    method: 'post',
    data: listQuery
  })
}

export function create(info) {
  return fetch({
    url: '/process/advorder/create',
    method: 'post',
    data: info
  })
}

export function update(info) {
  return fetch({
    url: '/process/advorder/update',
    method: 'post',
    data: info
  })
}

export function deleted(list) {
  return fetch({
    url: '/process/advorder/delete',
    method: 'post',
    data: list
  })
}

export function listDetail(listQuery) {
  return fetch({
    url: '/process/advorder/detailqueryPage',
    method: 'post',
    data: listQuery
  })
}
