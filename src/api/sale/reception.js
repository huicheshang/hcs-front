import fetch from '@/utils/fetch'

export function list(listQuery) {
  return fetch({
    url: '/presale/reception/queryPage',
    method: 'post',
    data: listQuery
  })
}

export function create(info) {
  return fetch({
    url: '/presale/reception/create',
    method: 'post',
    data: info
  })
}

export function update(info) {
  return fetch({
    url: '/presale/reception/update',
    method: 'post',
    data: info
  })
}

export function deleted(list) {
  return fetch({
    url: '/presale/reception/delete',
    method: 'post',
    data: list
  })
}


export function listDetail(listQuery) {
  return fetch({
    url: '/presale/reception/detailqueryPage',
    method: 'post',
    data: listQuery
  })
}

export function changeOrderType(list) {
  return fetch({
    url: '/presale/reception/changeOrderType',
    method: 'post',
    data: list
  })
}
