import fetch from '@/utils/fetch'

export function list(listQuery) {
  return fetch({
    url: '/process/saleorder/queryPage',
    method: 'post',
    data: listQuery
  })
}

export function create(info) {
  return fetch({
    url: '/process/saleorder/create',
    method: 'post',
    data: info
  })
}

export function update(info) {
  return fetch({
    url: '/process/saleorder/update',
    method: 'post',
    data: info
  })
}

export function deleted(list) {
  return fetch({
    url: '/process/saleorder/delete',
    method: 'post',
    data: list
  })
}

export function listDetail(listQuery) {
  return fetch({
    url: '/process/saleorder/detailqueryPage',
    method: 'post',
    data: listQuery
  })
}
