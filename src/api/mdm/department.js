import fetch from '@/utils/fetch'
var qs = require('qs')

export function listDepartment(listQuery) {
  return fetch({
    url: '/department/queryPage',
    method: 'post',
    data: listQuery
  })
}

export function create(info) {
  return fetch({
    url: '/department/create',
    method: 'post',
    data: info
  })
}

export function update(info) {
  return fetch({
    url: '/department/update',
    method: 'post',
    data: info
  })
}

export function deleted(list) {
  return fetch({
    url: '/department/delete',
    method: 'post',
    data: list
  })
}

