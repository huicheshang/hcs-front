import fetch from '@/utils/fetch'


export function listPost(listQuery) {
  return fetch({
    url: '/post/queryPage',
    method: 'post',
    data: listQuery
  })
}

export function createPost(info) {
  return fetch({
    url: '/post/create',
    method: 'post',
    data: info
  })
}

export function updatePost(info) {
  return fetch({
    url: '/post/update',
    method: 'post',
    data: info
  })
}

export function deletePost(list) {
  return fetch({
    url: '/post/delete',
    method: 'post',
    data: list
  })
}
