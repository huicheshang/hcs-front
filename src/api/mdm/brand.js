import fetch from '@/utils/fetch'
var qs = require('qs')

export function tree() {
  return fetch({
    url: '/brand/tree',
    method: 'get'
  })
}

export function treeChildren(id) {
  return fetch({
    url: '/series/treeChildren',
    method: 'get',
    params: { id }
  })
}

export function listVehicles(listQuery) {
  return fetch({
    url: '/vehicle/queryPage',
    method: 'post',
    data: listQuery
  })
}

export function createBrand(info) {
  return fetch({
    url: '/brand/create',
    method: 'post',
    data: info
  })
}

export function updateBrand(info) {
  return fetch({
    url: '/brand/update',
    method: 'post',
    data: info
  })
}

export function deleteBrand(list) {
  return fetch({
    url: '/brand/delete',
    method: 'post',
    data: list
  })
}

export function getAllBrands() {
  return fetch({
    url: '/brand/all',
    method: 'get'
  })
}

export function createSeries(info) {
  return fetch({
    url: '/series/create',
    method: 'post',
    data: info
  })
}

export function updateSeries(info) {
  return fetch({
    url: '/series/update',
    method: 'post',
    data: info
  })
}

export function deleteSeries(list) {
  return fetch({
    url: '/series/delete',
    method: 'post',
    data: list
  })
}

export function queryAirSuction() {
  return fetch({
    url: '/vehicle/queryAirSuction',
    method: 'get'
  })
}

export function queryPowType() {
  return fetch({
    url: '/vehicle/queryPowType',
    method: 'get'
  })
}

export function queryDriverMode() {
  return fetch({
    url: '/vehicle/queryDriverMode',
    method: 'get'
  })
}

export function createModel(info) {
  return fetch({
    url: '/vehicle/create',
    method: 'post',
    data: info
  })
}

export function updateModel(info) {
  return fetch({
    url: '/vehicle/update',
    method: 'post',
    data: info
  })
}

export function deleteModel(list) {
  return fetch({
    url: '/vehicle/delete',
    method: 'post',
    data: list
  })
}

export function getAllSeries(id) {
  return fetch({
    url: '/series/all',
    method: 'get',
    params: { id }
  })
}


