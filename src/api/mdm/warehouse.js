import fetch from '@/utils/fetch'
var qs = require('qs')

export function listwarehouse(listQuery) {
  return fetch({
    url: '/warehouse/queryPage',
    method: 'post',
    data: listQuery
  })
}

export function create(info) {
  return fetch({
    url: '/warehouse/create',
    method: 'post',
    data: info
  })
}

export function update(info) {
  return fetch({
    url: '/warehouse/update',
    method: 'post',
    data: info
  })
}

export function deleted(list) {
  return fetch({
    url: '/warehouse/delete',
    method: 'post',
    data: list
  })
}

export function queryWhType() {
  return fetch({
    url: '/warehouse/queryWhType',
    method: 'get'
  })
}

export function queryWhStyle() {
  return fetch({
    url: '/warehouse/queryWhStyle',
    method: 'get'
  })
}

export function listwarehouseLoc(listQuery) {
  return fetch({
    url: '/warehouseLocation/queryPage',
    method: 'post',
    data: listQuery
  })
}

export function createLoc(info) {
  return fetch({
    url: '/warehouseLocation/create',
    method: 'post',
    data: info
  })
}

export function updateLoc(info) {
  return fetch({
    url: '/warehouseLocation/update',
    method: 'post',
    data: info
  })
}

export function deletedLoc(list) {
  return fetch({
    url: '/warehouseLocation/delete',
    method: 'post',
    data: list
  })
}

export function queryWhLocType() {
  return fetch({
    url: '/warehouseLocation/queryWhLocType',
    method: 'get'
  })
}

export function listAll() {
  return fetch({
    url: '/warehouse/listAll',
    method: 'get'
  })
}
