import fetch from '@/utils/fetch'


export function listSupplier(listQuery) {
  return fetch({
    url: '/supplier/queryPage',
    method: 'post',
    data: listQuery
  })
}

export function createSupplier(info) {
  return fetch({
    url: '/supplier/create',
    method: 'post',
    data: info
  })
}

export function updateSupplier(info) {
  return fetch({
    url: '/supplier/update',
    method: 'post',
    data: info
  })
}

export function deleteSupplier(list) {
  return fetch({
    url: '/supplier/delete',
    method: 'post',
    data: list
  })
}
