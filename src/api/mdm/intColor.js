import fetch from '@/utils/fetch'


export function listintColors(listQuery) {
  return fetch({
    url: '/intcolor/queryPage',
    method: 'post',
    data: listQuery
  })
}

export function createIntColor(info) {
  return fetch({
    url: '/intcolor/create',
    method: 'post',
    data: info
  })
}

export function updateIntColor(info) {
  return fetch({
    url: '/intcolor/update',
    method: 'post',
    data: info
  })
}

export function deleteIntColor(list) {
  return fetch({
    url: '/intcolor/delete',
    method: 'post',
    data: list
  })
}
