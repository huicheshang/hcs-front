import fetch from '@/utils/fetch'

export function listshop(listQuery) {
  return fetch({
    url: '/shop/queryPage',
    method: 'post',
    data: listQuery
  })
}
