import fetch from '@/utils/fetch'


export function listExtColors(listQuery) {
  return fetch({
    url: '/extcolor/queryPage',
    method: 'post',
    data: listQuery
  })
}

export function createExtColor(info) {
  return fetch({
    url: '/extcolor/create',
    method: 'post',
    data: info
  })
}

export function updateExtColor(info) {
  return fetch({
    url: '/extcolor/update',
    method: 'post',
    data: info
  })
}

export function deleteExtColor(list) {
  return fetch({
    url: '/extcolor/delete',
    method: 'post',
    data: list
  })
}
