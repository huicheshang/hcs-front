import fetch from '@/utils/fetch'


export function listplan(listQuery) {
  return fetch({
    url: '/purchasePlan/queryPage',
    method: 'post',
    data: listQuery
  })
}

export function detailplan(info) {
  return fetch({
    url: '/purchasePlan/detailPage',
    method: 'post',
    data: info
  })
}

export function execplan(info) {
  return fetch({
    url: '/purchasePlan/execPage',
    method: 'post',
    data: info
  })
}

export function optplan(info) {
  return fetch({
    url: '/purchasePlan/optPage',
    method: 'post',
    data: info
  })
}

export function listdeplan(info) {
  return fetch({
    url: '/purchasePlan/detailEditPage',
    method: 'post',
    data: info
  })
}


export function createPlan(info) {
  return fetch({
    url: '/purchasePlan/create',
    method: 'post',
    data: info
  })
}


export function updatePlan(info) {
  return fetch({
    url: '/purchasePlan/update',
    method: 'post',
    data: info
  })
}


export function deletePlan(list) {
  return fetch({
    url: '/purchasePlan/delete',
    method: 'post',
    data: list
  })
}
