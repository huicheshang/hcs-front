export default [
  {
    path: '/sale/presale',
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    name: '售前跟踪',
    icon: 'chart',
    children: [{
      path: 'reception',
      component: (resolve) => require(['@/views/sale/presale/reception'], resolve),
      name: '展厅接待',
      icon: 'chart',
    },
      {
        path: 'candidate',
        component: (resolve) => require(['@/views/sale/presale/candidate'], resolve),
        name: '意向客户',
        icon: 'chart',
      },
      {
        path: 'tips',
        component: (resolve) => require(['@/views/sale/presale/tips'], resolve),
        name: '意向跟踪',
        icon: 'chart',
      }]
  },
  {
    path: '/sale/process',
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    name: '销售管理',
    icon: 'chart',
    children: [{
      path: 'advorder',
      component: (resolve) => require(['@/views/sale/process/advorder'], resolve),
      name: '预订单',
      icon: 'chart',
    },
      {
        path: 'saleorder',
        component: (resolve) => require(['@/views/sale/process/saleorder'], resolve),
        name: '销售订单',
        icon: 'chart',
      }
    ]
  },
  {
    path: '/sale/vas',
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    name: '其他服务',
    icon: 'chart',
    children: [{
      path: 'accessory',
      component: (resolve) => require(['@/views/sale/vas/accessory'], resolve),
      name: '精品加装'
    },
      {
        path: 'insurance',
        component: (resolve) => require(['@/views/sale/vas/insurance'], resolve),
        name: '保险服务'
      }
      ,
      {
        path: 'commission',
        component: (resolve) => require(['@/views/sale/vas/commission'], resolve),
        name: '代办服务'
      },
      {
        path: 'misc',
        component: (resolve) => require(['@/views/sale/vas/misc'], resolve),
        name: '其它服务'
      }
    ]
  },
  {
    path: '/inventory/inbound',
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    name: '整车入库',
    icon: 'chart',
    children: [{
      path: 'pur',
      component: (resolve) => require(['@/views/inventory/inbound/pur'], resolve),
      name: '采购入库'
      },
      {
        path: 'allot',
        component: (resolve) => require(['@/views/inventory/inbound/allot'], resolve),
        name: '调拨入库'
      }
    ]
  },
  {
    path: '/inventory/outbound',
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    name: '整车出库',
    icon: 'chart',
    children: [{
      path: 'sale',
      component: (resolve) => require(['@/views/inventory/outbound/sale'], resolve),
      name: '销售出库'
    },
      {
        path: 'allot',
        component: (resolve) => require(['@/views/inventory/outbound/allot'], resolve),
        name: '调拨出库'
      }
      ,
      {
        path: 'recall',
        component: (resolve) => require(['@/views/inventory/outbound/recall'], resolve),
        name: '采购退回'
      },
      {
        path: 'misc',
        component: (resolve) => require(['@/views/inventory/outbound/misc'], resolve),
        name: '其他出库'
      }
    ]
  },
  {
    path: '/inventory/mgmt',
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    name: '库存管理',
    icon: 'chart',
    children: [{
      path: 'query',
      component: (resolve) => require(['@/views/inventory/mgmt/query'], resolve),
      name: '库存查询'
    },
      {
        path: 'domove',
        component: (resolve) => require(['@/views/inventory/mgmt/domove'], resolve),
        name: '店内移车'
      },
      {
        path: 'exb',
        component: (resolve) => require(['@/views/inventory/mgmt/exb'], resolve),
        name: '展车管理'
      },
      {
        path: 'demo',
        component: (resolve) => require(['@/views/inventory/mgmt/demo'], resolve),
        name: '试驾车辆管理'
      }
    ]
  },
  {
    path: '/crm/archive',
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    name: '客户档案',
    icon: 'chart',
    children: [{
      path: 'ordinary',
      component: (resolve) => require(['@/views/crm/archive/ordinary'], resolve),
      name: '普通客户'
    },
      {
        path: 'ka',
        component: (resolve) => require(['@/views/crm/archive/ka'], resolve),
        name: '大客户'
      }
    ]
  },
  {
    path: '/purchase/plan',
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    name: '采购计划',
    icon: 'chart',
    children: [{
      path: 'plan',
      component: (resolve) => require(['@/views/purchase/plan/plan'], resolve),
      name: '计划管理'
    }]
  },
  {
    path: '/purchase/order',
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    name: '采购订单',
    icon: 'chart',
    children: [{
      path: 'order',
      component: (resolve) => require(['@/views/purchase/order/order'], resolve),
      name: '采购下单'
    }]
  },

  {
    path: '/mdm/common',
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    name: '公共数据',
    icon: 'chart',
    children: [{
      path: 'wm',
      component: (resolve) => require(['@/views/mdm/common/wm'], resolve),
      name: '仓库设置'
    },
      {
        path: 'shop',
        component: (resolve) => require(['@/views/mdm/common/shop'], resolve),
        name: '门店配置'
      },
      {
        path: 'department',
        component: (resolve) => require(['@/views/mdm/common/department'], resolve),
        name: '部门配置'
      },
      {
        path: 'post',
        component: (resolve) => require(['@/views/mdm/common/post'], resolve),
        name: '岗位配置'
      },
      {
        path: 'supplier',
        component: (resolve) => require(['@/views/mdm/common/supplier'], resolve),
        name: '厂商设置'
      },
      {
        path: 'retailor',
        component: (resolve) => require(['@/views/mdm/common/retailor'], resolve),
        name: '经销商设置'
      },
    ]
  },
  {
    path: '/mdm/car',
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    name: '车辆数据',
    icon: 'chart',
    children: [{
      path: 'brand',
      component: (resolve) => require(['@/views/mdm/car/brand'], resolve),
      name: '品牌车系车型'
    },
      {
        path: 'extcolor',
        component: (resolve) => require(['@/views/mdm/car/extcolor'], resolve),
        name: '外观颜色'
      },
      {
        path: 'intcolor',
        component: (resolve) => require(['@/views/mdm/car/intcolor'], resolve),
        name: '内饰颜色'
      }
    ]
  },
  {
    path: '/sys/admin',
    component: (resolve) => require(['@/views/layout/Layout'], resolve),
    name: '系统管理',
    icon: 'chart',
    children: [{
      path: 'org',
      component: (resolve) => require(['@/views/sys/admin/org'], resolve),
      name: '机构管理'
    },
      {
        path: 'role',
        component: (resolve) => require(['@/views/sys/admin/role'], resolve),
        name: '角色管理'
      },
      {
        path: 'user',
        component: (resolve) => require(['@/views/sys/admin/user'], resolve),
        name: '用户管理'
      },
      {
        path: 'changepwd',
        component: (resolve) => require(['@/views/sys/admin/changepwd'], resolve),
        name: '修改密码'
      },
      {
        path: 'template',
        component: (resolve) => require(['@/views/sys/admin/template'], resolve),
        name: '模版下载'
      },
      {
        path: 'log',
        component: (resolve) => require(['@/views/sys/admin/log'], resolve),
        name: '系统日志'
      }
    ]
  },
  {path: '*', name: 'error', hidden: true, redirect: '/404\'/404\''}
]
